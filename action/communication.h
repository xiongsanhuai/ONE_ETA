#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

#include "main.h"
#include "tim.h"
#include "usart.h"
#include <rthw.h>
#include <rtthread.h>
#include "action.h"

#define  LENGTH 1024//缓存数组大小
#define ESP_LENGTH 23  //17
#define ORDER_NUM 16 //订单窗口和菜品位数

typedef struct
{
	u16 conter;
	u8 buffer[LENGTH];
	u8 rx_buffer[1];
	u8 flag;
}MY_USART;

typedef struct Cus_Link
{
	u8 num;//头结点放链表数 其他放订单号
	u8 door_num;//门编号
	struct Cus_Link *next;//指针域
}cus_link;

extern MY_USART my_usart1;
extern MY_USART my_usart2;
extern u16 esp_deal[ESP_LENGTH];
extern cus_link *my_link;

void usart1_send(void);
void f407_deal(void);
void esp8266_deal(void);
void link_Init(void);

#endif



