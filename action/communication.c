#include "communication.h"
#include "stdlib.h"
#include <rthw.h>
#include <rtthread.h>
MY_USART my_usart1={0};
MY_USART my_usart2={0};
u16 esp_deal[ESP_LENGTH] = {0};
u32 menu_num = 0;

cus_link *my_link;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	rt_interrupt_enter();
	//407相互通信
	if(huart->Instance == USART1)
	{
		HAL_UART_Receive_IT(&huart1,my_usart1.rx_buffer,1);
		if(!(my_usart1.conter&0x8000))//未接收到帧尾
		{
			if(my_usart1.conter&0x4000)//接收到帧头 数据缓存
			{
				if(my_usart1.rx_buffer[0] == 0xbb)//接收帧尾
				{
					my_usart1.conter |= 0x8000;
					my_usart1.flag = 1;
				}
				else
				{
					my_usart1.buffer[my_usart1.conter&0x3fff] = my_usart1.rx_buffer[0];
					my_usart1.conter++;
					if((my_usart1.conter&0x3fff)>(LENGTH-1))
						my_usart1.conter = 0;//接收错误
				}
			}
			else//准备接收帧头
			{
				if(my_usart1.rx_buffer[0] == 0xaa)
				my_usart1.conter |= 0x4000;
			}
		}
	}
	//8266通信
	if(huart->Instance == USART2)
	{
		HAL_UART_Receive_IT(&huart2,my_usart2.rx_buffer,1);
		if(my_usart2.conter&0x2000)//接收到帧头
		{
			if(!(my_usart2.conter&0x8000))//未接收\n
			{
				if(my_usart2.conter&0x4000)//接收到\r
				{
					if(my_usart2.rx_buffer[0] == '\n')
					{
						my_usart2.conter|=0x8000;
						my_usart2.flag = 1;
					}
					else my_usart2.conter = 0;//接收错误
				}
				else//未接收到\r
				{
					if(my_usart2.rx_buffer[0] == '\r')//准备接收\r
					{
						my_usart2.conter|=0x4000;
					}
					else
					{
						my_usart2.buffer[my_usart2.conter&0x1fff] = my_usart2.rx_buffer[0];
						my_usart2.conter++;
						if((my_usart2.conter&0x1fff)>LENGTH-1)my_usart2.conter = 0;//接收错误
					}
				}
			}
		}
		else//准备接收帧头
		{
			if(my_usart2.rx_buffer[0] == '#'||my_usart2.rx_buffer[0] == '@')
				my_usart2.conter |= 0x2000;
		}
	}
	rt_interrupt_leave();
}
//发送给407
void usart1_send(void)
{
	u8 buffer_send[255]={0};
	u8 num = 0;
	if(esp_deal[ESP_LENGTH-5]!=0x00)//订单
	{
		buffer_send[num++] = 0xaa;
		for(u8 i= 0;i<ESP_LENGTH-3;i++)
		{
			buffer_send[num++] = esp_deal[i];
		}
		cus_link *temp = my_link;
		for(u8 i = 0;i<my_link->num;i++)
		{
			temp = temp->next;//遍历到表尾
		}
		buffer_send[num++] = temp->num;
		buffer_send[num++] = 0xbb;
		HAL_UART_Transmit(&huart1,buffer_send,num,0xffff);
	}
	else//取餐
	{
		buffer_send[num++] = 0xcc;
		buffer_send[num++] = esp_deal[0];
		buffer_send[num++] = 0xdd;
		HAL_UART_Transmit(&huart1,buffer_send,num,0xffff);
		
	}
}

//处理407数据
void f407_deal(void)
{
//	if(my_usart1.buffer[0] == 0xFF)
//		FLAG_FOOD = 1;
	cus_link *p = my_link;
	while(p->next)
	{
		if(p->next->num == my_usart1.buffer[0])//有此订单
		{
			FLAG_FOOD = 1;
			FLAG_WINDOW = p->next->door_num;//几号窗口
			cus_link *del = p->next;
			p->next = p->next->next;
			rt_free(del);
			my_link->num--;
			break;
		}
		p = p->next;
	}
}

//8266接收数据处理
void esp8266_deal(void)
{
	u16 i = 0;
	if((my_usart2.conter&0x1fff)>1)//接收到#
	{
		for(i = 0;i<ORDER_NUM;i++)
		{
			esp_deal[i] = (my_usart2.buffer[i]-48);
		}
		esp_deal[i++] = ((my_usart2.buffer[(my_usart2.conter&0x1fff)-8]-48)*10)+(my_usart2.buffer[(my_usart2.conter&0x1fff)-7]-48);//计算总价
		esp_deal[i++] = ((my_usart2.buffer[(my_usart2.conter&0x1fff)-6]-48)*10)+(my_usart2.buffer[(my_usart2.conter&0x1fff)-5]-48);//计算时间 小时
		esp_deal[i++] = ((my_usart2.buffer[(my_usart2.conter&0x1fff)-4]-48)*10)+(my_usart2.buffer[(my_usart2.conter&0x1fff)-3]-48);//计算时间 分钟
		esp_deal[i++] = ((my_usart2.buffer[(my_usart2.conter&0x1fff)-2]-48)*10)+(my_usart2.buffer[(my_usart2.conter&0x1fff)-1]-48);//计算时间 秒
		//FLAG_WINDOW = esp_deal[0];//几号窗口
		
		
		cus_link *temp = my_link;
		for(u8 i = 0;i<my_link->num;i++)
		{
			temp = temp->next;//遍历到表尾
		}
		 //创建插入结点c
    cus_link * c=(cus_link*)rt_malloc(sizeof(cus_link));
		c->door_num = esp_deal[0];
		c->num = ++menu_num;
		//向链表插入结点
		c->next = NULL;
		temp->next = c;
		my_link->num++;
	}
	else//接收到@
	{
		esp_deal[0] = (my_usart2.buffer[0]-48);
		FLAG_OPEN = esp_deal[0];
	}
}

//头结点初始化
void link_Init(void)
{
	my_link = (cus_link *)rt_malloc(sizeof(cus_link));
	my_link->num = 0;
	my_link->door_num = 0;
	my_link->next = NULL;
}

