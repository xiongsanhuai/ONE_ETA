#include "action.h"

u8 FLAG_PWM1 = 0,FLAG_PWM2 = 0,FLAG_PWM3 = 0,FLAG_PWM4 = 0;//pwm标志  0为关闭 1为打开
u8 FLAG_WINDOW = 0;//去哪个窗口和开窗口
u8 FLAG_FOOD = 0;  //送餐标志
u8 FLAG_OPEN = 0;  //开窗口
//u8 FLAG_A1 = 0,FLAG_A2 = 0,FLAG_A3 = 0,FLAG_A4 = 0;

//改变pwm频率
HAL_StatusTypeDef change_pwm(TIM_HandleTypeDef *htim,u16 arr,u16 psc)
{
	htim->Instance->ARR=arr;  	//设定计数器自动重装值 
	htim->Instance->PSC=psc;  	//预分频器	
	htim->Instance->CCR1 = (arr+1)/2-1;
	return HAL_OK;
}


//开门
DoorNum Open_Door(u8 num)
{
	switch(num)
	{
		case 1:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8,GPIO_PIN_RESET);
			rt_thread_delay(500);
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_1;
		case 2:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9,GPIO_PIN_RESET);
			rt_thread_delay(500);
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_2;
		case 3:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10,GPIO_PIN_RESET);
			rt_thread_delay(500);
		  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_3;
		case 4:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11,GPIO_PIN_RESET);
			rt_thread_delay(500);
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_4;
		case 5:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12,GPIO_PIN_RESET);
			rt_thread_delay(500);
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_5;
		case 6:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13,GPIO_PIN_RESET);
			rt_thread_delay(500);
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13,GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_6;
		default:
			HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_12|GPIO_PIN_13, GPIO_PIN_SET);
			FLAG_OPEN = 0;
			return Door_error;
	}
}

//tim回调函数
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	rt_interrupt_enter();
	if(htim->Instance == TIM4)
	{
		if(__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC1) != RESET)          
    { 
         __HAL_TIM_CLEAR_FLAG(&htim4, TIM_FLAG_CC1);                
          
         HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_1);                   
         
         HAL_TIM_Base_Stop_IT(&htim4);
		}
		if(__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC2) != RESET)          
    { 
         __HAL_TIM_CLEAR_FLAG(&htim4, TIM_FLAG_CC2);                
          
         HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_2);                   
         
         HAL_TIM_Base_Stop_IT(&htim4);
		}
		if(__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC3) != RESET)          
    { 
         __HAL_TIM_CLEAR_FLAG(&htim4, TIM_FLAG_CC3);                
          
         HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_3);                   
         
         HAL_TIM_Base_Stop_IT(&htim4);
		}
		if(__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC4) != RESET)          
    { 
         __HAL_TIM_CLEAR_FLAG(&htim4, TIM_FLAG_CC4);                
          
         HAL_TIM_PWM_Stop_IT(&htim3, TIM_CHANNEL_4);                   
         
         HAL_TIM_Base_Stop_IT(&htim4);
		}
		FLAG_PWM1 = 1;FLAG_PWM2 = 1;FLAG_PWM3 = 1;FLAG_PWM4 = 1;
	}
	rt_interrupt_leave();
}

//输出脉冲数
void Output_Pwm(float pwmvalue)
{
		if(FLAG_PWM1 == 1)
		{
			FLAG_PWM1 = 0;
			//__HAL_TIM_SET_AUTORELOAD(&htim4,pwmvalue*PWMBASE-1); //设置自动装载值
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_1);
		}
		if(FLAG_PWM2 == 1)
		{
			FLAG_PWM2 = 0;
			//__HAL_TIM_SET_AUTORELOAD(&htim4,pwmvalue*PWMBASE-1); //设置自动装载值
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_2);
		}
		if(FLAG_PWM3 == 1)
		{
			FLAG_PWM3 = 0;
			//__HAL_TIM_SET_AUTORELOAD(&htim4,pwmvalue*PWMBASE-1); //设置自动装载值
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_3);
		}
		if(FLAG_PWM4 == 1)
		{
			FLAG_PWM4 = 0;
			//__HAL_TIM_SET_AUTORELOAD(&htim4,pwmvalue*PWMBASE-1); //设置自动装载值
			HAL_TIM_Base_Start_IT(&htim4);
			HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_4);
		}
		__HAL_TIM_SET_AUTORELOAD(&htim4,pwmvalue*PWMBASE-1); //设置自动装载值
}
//控制电机 圈数 几号电机  方向GPIO_PIN_SET下降向左  GPIO_PIN_RESET 上升向右开启方向GPIO_PIN_RESET关闭方向GPIO_PIN_SET
void dianji_control(float pwmvalue,u8 who,GPIO_PinState fx,GPIO_PinState contorl)
{
	switch(who)
	{
		case 1:
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_8,fx);
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_10,contorl);
			break;
		case 2:
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_9,fx);
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_11,contorl);
			break;
		case 3:
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_10,fx);
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12,contorl);
			break;
		case 4:
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_11,fx);
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_13,contorl);
			break;
		default:
			break;
	}
	Output_Pwm(pwmvalue);
}




