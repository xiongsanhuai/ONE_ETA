#ifndef __ACTION_H
#define __ACTION_H

#include "main.h"
#include "tim.h"
#include "usart.h"
#include <rthw.h>
#include <rtthread.h>


#define PWMBASE 200//�������������

typedef enum
{
	Door_error = 0,
	Door_1 ,
	Door_2,
	Door_3,
	Door_4,
	Door_5,
	Door_6
}DoorNum;


extern u8 FLAG_WINDOW;
extern u8 FLAG_FOOD;
extern u8 FLAG_OPEN;

HAL_StatusTypeDef change_pwm(TIM_HandleTypeDef *htim,u16 arr,u16 psc);
DoorNum Open_Door(u8 num);
void Output_Pwm(float pwmvalue);
void dianji_control(float pwmvalue,u8 who,GPIO_PinState fx,GPIO_PinState contorl);

#endif


