/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <rthw.h>
#include <rtthread.h>
#include "stdio.h"
#include "action.h"
#include "communication.h"
#include "string.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
void go_window(void);
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */


ALIGN(RT_ALIGN_SIZE)
/****************线程控制块***********************/
static struct rt_thread FoodDelivery_thread;       //送餐
static struct rt_thread reset_thread;             //步进复位
static struct rt_thread deal_thread;           //串口数据处理
static struct rt_thread open_thread;            //窗口打开
static struct rt_thread hongwai_thread;         //红外检测


ALIGN(RT_ALIGN_SIZE)                       			//字节对齐
/****************线程栈***********************/
static rt_uint8_t rt_FoodDelivery_thread_stack[1024];       //送餐
static rt_uint8_t rt_reset_thread_stack[1024];             //步进复位
static rt_uint8_t rt_deal_thread_stack[1024];
static rt_uint8_t rt_open_thread_stack[1024];
static rt_uint8_t rt_hongwai_thread_stack[1024];

/****************线程栈***********************/
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

static void reset_entry(void *param)
{
	while(1)
	{
//		dianji_control(1,1,GPIO_PIN_SET,GPIO_PIN_RESET);//z轴复位
//		dianji_control(1,2,GPIO_PIN_SET,GPIO_PIN_RESET);
//		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_6) == 0)
//		{
//			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
//			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
//			dianji_control(1,3,GPIO_PIN_RESET,GPIO_PIN_RESET);//x轴复位
//			if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_7) == 0)
//			{
//				dianji_control(0,3,GPIO_PIN_SET,GPIO_PIN_SET);
//				dianji_control(1,4,GPIO_PIN_RESET,GPIO_PIN_RESET);//y轴复位
//				if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_8) == 0)
//				{
//					dianji_control(0,4,GPIO_PIN_SET,GPIO_PIN_SET);
//					rt_thread_suspend(&reset_thread);//线程挂起
//					rt_schedule();
//				}
//			}
//		}
		dianji_control(1,4,GPIO_PIN_RESET,GPIO_PIN_RESET);//y轴复位
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_8) == 0)
		{
			dianji_control(0,4,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(1,3,GPIO_PIN_RESET,GPIO_PIN_RESET);//x轴复位
			if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_7) == 0)
			{
				dianji_control(0,3,GPIO_PIN_SET,GPIO_PIN_SET);
				dianji_control(1,1,GPIO_PIN_SET,GPIO_PIN_RESET);//z轴复位
				dianji_control(1,2,GPIO_PIN_SET,GPIO_PIN_RESET);
				if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_6) == 0)
				{
					dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
					dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
					__HAL_TIM_SET_AUTORELOAD(&htim4,0);
					rt_thread_suspend(&reset_thread);//线程挂起
					rt_schedule();
				}
			}
		}
		//rt_thread_delay(5);
	}
}


static void FoodDelivery_entry(void *param)
{
	while(1)
	{
		if(FLAG_FOOD)
		{
			FLAG_FOOD = 0;
			go_window();
		}
//		go_window();
		rt_thread_delay(5);
	}
}


static void deal_entry(void *param)
{
	while(1)
	{
		if(my_usart1.flag)//407处理
		{
			f407_deal();
			memset(my_usart1.buffer,0,sizeof(my_usart1.buffer));
			my_usart1.conter = 0;
			my_usart1.flag = 0;
		}
		if(my_usart2.flag)//8266处理
		{
			//HAL_UART_Transmit(&huart1,(u8 *)"666",4,0xffff);
			esp8266_deal();
			memset(my_usart2.buffer,0,sizeof(my_usart2.buffer));
			my_usart2.conter = 0;
			my_usart2.flag = 0;
			usart1_send();
			rt_thread_delay(500);
			usart1_send();
			memset(esp_deal,0,sizeof(esp_deal));
		}
		
		rt_thread_delay(5);
	}
	
}

static void open_entry(void *param)
{
	while(1)
	{
		Open_Door(FLAG_OPEN);
		rt_thread_delay(5);
	}
}

static void hongwai_entry(void *param)
{
	static u8 time_1=0;
	static u8	time_2=0;
	static u8 time_3=0;
	static u8	time_4=0;
	static u8 time_5=0;
	static u8	time_6=0;
	static u8 flag = 0;
	static u8 last_flag = 0;
	u8 send_buffer[8] = {0xab,0x00,0x00,0x00,0x00,0x00,0x00,0xcd};
	while(1)
	{
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_2) == 1)
		{
			if(!(flag&0x01))
			{
				time_1++;
				
			}
		}
		else
		{
			if((flag&0x01))
			{
				//send_buffer[1] = 0x01;
				send_buffer[1] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xfe;
			time_1 = 0;
		}
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_3) == 1)
		{
			if(!(flag&0x02))
				time_2++;
		}
		else
		{
			if((flag&0x02))
			{
				//send_buffer[1] = 0x02;
				send_buffer[2] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xfd;
			time_2 = 0;
		}
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_4) == 1)
		{
			if(!(flag&0x04))
				time_3++;
		}
		else
		{
			if((flag&0x04))
			{
				//send_buffer[1] = 0x03;
				send_buffer[3] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xfb;
			time_3 = 0;
		}
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_5) == 1)
		{
			if(!(flag&0x08))
				time_4++;
		}
		else
		{
			if((flag&0x08))
			{
				//send_buffer[1] = 0x04;
				send_buffer[4] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xf7;
			time_4 = 0;
		}
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_6) == 1)
		{
			if(!(flag&0x10))
			time_5++;
		}
		else
		{
			if((flag&0x10))
			{
				//send_buffer[1] = 0x05;
				send_buffer[5] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xef;
			time_5 = 0;
		}
		if(HAL_GPIO_ReadPin(GPIOG,GPIO_PIN_7) == 1)
		{
			if(!(flag&0x20))
			time_6++;
		}
		else
		{
			if((flag&0x20))
			{
				//end_buffer[1] = 0x06;
				send_buffer[6] = 0x00;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag &= 0xdf;
			time_6 = 0;
		}
		if(time_1>=10)
		{
			if(!(flag&0x01))
			{
				//send_buffer[1] = 0x01;
				send_buffer[1] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x01;
		}
		if(time_2>=10)
		{
			if(!(flag&0x02))
			{
				//send_buffer[1] = 0x02;
				send_buffer[2] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x02;
		}
		if(time_3>=10)
		{
			if(!(flag&0x04))
			{
				//send_buffer[1] = 0x03;
				send_buffer[3] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x04;
		}
		if(time_4>=10)
		{
			if(!(flag&0x08))
			{
				//send_buffer[1] = 0x04;
				send_buffer[4] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x08;
		}
		if(time_5>=10)
		{
			if(!(flag&0x10))
			{
				//send_buffer[1] = 0x05;
				send_buffer[5] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x10;
		}
		if(time_6>=10)
		{
			if(!(flag&0x20))
			{
				//send_buffer[1] = 0x06;
				send_buffer[6] = 0x01;
				//HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
			}
			flag |= 0x20;
		}
		if(last_flag != flag)
		{
			HAL_UART_Transmit(&huart1,send_buffer,sizeof(send_buffer),1000);
		}
		last_flag = flag;
		rt_thread_delay(1000);
	}
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
	

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM4_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
	link_Init();
	HAL_UART_Receive_IT(&huart1,my_usart1.rx_buffer,1);
	HAL_UART_Receive_IT(&huart2,my_usart2.rx_buffer,1);
	HAL_TIM_Base_Start_IT(&htim4); 
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_4);
	__HAL_TIM_SET_AUTORELOAD(&htim4,0);
	change_pwm(&htim3,840-1,200-1);
//	FLAG_WINDOW = 1;
//	dianji_control(10,3,GPIO_PIN_RESET,GPIO_PIN_RESET);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	// 创建静态线程
    rt_thread_init(&reset_thread,                 //线程控制块
                   "reset",                       //线程名字，在shell里面可以看到
                   reset_entry,            //线程入口函数
                   RT_NULL,                      //线程入口函数参数
                   &rt_reset_thread_stack[0],     //线程栈起始地址
                   sizeof(rt_reset_thread_stack), //线程栈大小
                   3,                            //线程的优先级
                   20);                          //线程时间片
		rt_thread_startup(&reset_thread); //启动线程 开始调度		
	
	// 创建静态线程
    rt_thread_init(&FoodDelivery_thread,                 //线程控制块
                   "FoodDelivery",                       //线程名字，在shell里面可以看到
                   FoodDelivery_entry,            //线程入口函数
                   RT_NULL,                      //线程入口函数参数
                   &rt_FoodDelivery_thread_stack[0],     //线程栈起始地址
                   sizeof(rt_FoodDelivery_thread_stack), //线程栈大小
                   5,                            //线程的优先级
                   20);                          //线程时间片
		rt_thread_startup(&FoodDelivery_thread); //启动线程 开始调度	 
									 
	  // 创建静态线程
    rt_thread_init(&deal_thread,                 //线程控制块
                   "deal",                       //线程名字，在shell里面可以看到
                   deal_entry,            //线程入口函数
                   RT_NULL,                      //线程入口函数参数
                   &rt_deal_thread_stack[0],     //线程栈起始地址
                   sizeof(rt_deal_thread_stack), //线程栈大小
                   5,                            //线程的优先级
                   20);                          //线程时间片
		rt_thread_startup(&deal_thread); //启动线程 开始调度
	
		// 创建静态线程
    rt_thread_init(&open_thread,                 //线程控制块
                   "open",                       //线程名字，在shell里面可以看到
                   open_entry,            //线程入口函数
                   RT_NULL,                      //线程入口函数参数
                   &rt_open_thread_stack[0],     //线程栈起始地址
                   sizeof(rt_deal_thread_stack), //线程栈大小
                   5,                            //线程的优先级
                   20);                          //线程时间片
		rt_thread_startup(&open_thread); //启动线程 开始调度
									 
		// 创建静态线程
    rt_thread_init(&hongwai_thread,                 //线程控制块
                   "hongwai",                       //线程名字，在shell里面可以看到
                   hongwai_entry,            //线程入口函数
                   RT_NULL,                      //线程入口函数参数
                   &rt_hongwai_thread_stack[0],     //线程栈起始地址
                   sizeof(rt_hongwai_thread_stack), //线程栈大小
                   5,                            //线程的优先级
                   20);                          //线程时间片 
		rt_thread_startup(&hongwai_thread); //启动线程 开始调度
	
	
  //while (1)
  //{
		//usart2_printf("ok!!!\r\n");
		//HAL_UART_Transmit(&huart2,(u8 *)"ok\r\n",6,100);
		//rt_thread_delay(500);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  //
	return 0;
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

//那个窗口
void go_window(void)
{
	__HAL_TIM_SET_AUTORELOAD(&htim4,0);
	TIM4->CNT = 0;
	switch(FLAG_WINDOW)
	{
		case 1:
		{
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(2000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(2,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(2,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			
			break;
		}
		case 2:
		{
			dianji_control(10,3,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(4000);
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(2000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(2,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(2,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			break;
		}
		case 3:
		{
			dianji_control(19,3,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(8000);
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(2000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(3,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(3,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			break;
		}
		case 4:
		{
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(23.5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(23.5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(12000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(2,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(2,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			break;
		}
		case 5:
		{
			dianji_control(10,3,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(4000);
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(23.5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(23.5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(12000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(2,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(2,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			break;
		}
		case 6:
		{
			dianji_control(19,3,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(8000);
			dianji_control(0,3,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(23.5,1,GPIO_PIN_RESET,GPIO_PIN_RESET);
			dianji_control(23.5,2,GPIO_PIN_RESET,GPIO_PIN_RESET);
			rt_thread_delay(13000);
			dianji_control(0,1,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_SET,GPIO_PIN_SET);
			dianji_control(3.5,4,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1300);
			dianji_control(0,4,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(3,1,GPIO_PIN_SET,GPIO_PIN_RESET);
			dianji_control(3,2,GPIO_PIN_SET,GPIO_PIN_RESET);
			rt_thread_delay(1500);
			dianji_control(0,1,GPIO_PIN_RESET,GPIO_PIN_SET);
			dianji_control(0,2,GPIO_PIN_RESET,GPIO_PIN_SET);
			FLAG_WINDOW = 0;
			rt_thread_delay(2000);
			rt_thread_resume(&reset_thread);//复位线程挂起
			break;
		}
		default:
			break;
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
